[![pipeline status](https://gitlab.pasteur.fr/bbrancot/django-basetheme-bootstrap/badges/master/pipeline.svg)](https://gitlab.pasteur.fr/bbrancot/django-basetheme-bootstrap/commits/master)
[![coverage report](https://gitlab.pasteur.fr/bbrancot/django-basetheme-bootstrap/badges/master/coverage.svg)](https://gitlab.pasteur.fr/bbrancot/django-basetheme-bootstrap/commits/master)

# django-basetheme-bootstrap

A module handling basic fonctionnality needed in django project such as base.html using bootstrap, account settings, preferences management, ...

# Quick start

1. Add "basetheme_bootstrap" to your INSTALLED_APPS setting like this (while not forgetting `crispy_forms`). Note that basetheme_bootstrap have to be before django.contrib.admin to override the default template for password reset.

```python
INSTALLED_APPS = [
    ...
    'crispy_forms',
    'crispy_bootstrap4',
    'basetheme_bootstrap',
    ...
    'django.contrib.admin',
]
```

2. Add the context_processors::

```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                ...
                'basetheme_bootstrap.context_processors.processors',
            ],
        },
    },
]
```

3. Adjust the configuration::


```python
################################################################################
# django-crispy-forms
################################################################################
CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_ALLOWED_TEMPLATE_PACKS = 'bootstrap4'

################################################################################
# basetheme_bootstrap
################################################################################
BASETHEME_BOOTSTRAP_TEMPLATE_LOCATION_PROJECT = "test_app_1"
BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_ENABLED = True
BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_LOCATION_APP = "test_app_1"
BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_NAME = "MyUserPreferences"
BASETHEME_BOOTSTRAP_USERNAME_IS_EMAIL = False
BASETHEME_BOOTSTRAP_FIRST_LAST_NAME_REQUIRED = False
BASETHEME_BOOTSTRAP_VALIDATE_EMAIL_BEFORE_ACTIVATION = False
BASETHEME_BOOTSTRAP_FAILED_LOGIN_EXPONENTIAL_BACKOFF_ENABLED = True
BASETHEME_BOOTSTRAP_FAILED_LOGIN_EXPONENTIAL_BACKOFF_GRADIENT = 1.47

################################################################################
```

3. Include the polls URLconf in your project urls.py like this::

```python
from django.urls import path, include

path('', include('basetheme_bootstrap.urls')),
```

3. Run `python manage.py migrate` to create the UserPreferences model if you decided to have one.

4. Make your templates extends base template from`basetheme_bootstrap`, or even better create your own base.html which extends the one from `basetheme_bootstrap`::

```html
{% extends "basetheme_bootstrap/base4.html" %}
```

# Cheat sheet

```bash
mkdir -p locale && python manage.py makemessages -l en -l fr --no-location && rm -rf ./locale
``` 