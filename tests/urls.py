from django import forms
from django.contrib import admin
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import path, include
from django.views import View
from crispy_forms.helper import FormHelper


def blabla(request):
    return HttpResponse("ee")


class FormExample(forms.Form):
    char_field = forms.CharField(
        help_text="bla bla",
    )
    texte_field = forms.CharField(
        widget=forms.widgets.Textarea(),
        help_text="bla bla",
    )
    select_field = forms.ChoiceField(
        choices=((1, 'a'), (2, 'b'),),
        widget=forms.widgets.Select(),
    )
    radio_field = forms.ChoiceField(
        choices=((1, 'a'), (2, 'b'),),
        widget=forms.widgets.RadioSelect(),
        help_text="bla bla",
    )
    bool_field = forms.BooleanField(
        required=False,
        help_text="bla bla",
    )
    helper = FormHelper()
    helper.use_custom_control = False


context = dict(
    form=FormExample(),
    form_2=FormExample(prefix="titi"),
    basetheme_bootstrap_example=True,
)


class BS4(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'test_app_1/mybase4.html', context=context)


class Pasteur(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'test_app_1/mybasePasteur.html', context=context)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', BS4.as_view(), name='home'),
    path('themePasteur', Pasteur.as_view()),
    path('', include('basetheme_bootstrap.urls')),
]
