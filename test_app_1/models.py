from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from basetheme_bootstrap import user_preferences


class ProtectedModel(models.Model):
    owner = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
    )

    # def get_absolute_url(self):
    #     return "rr"

class MyUserPreferences(user_preferences.UserPreferencesAbstractModel, models.Model):
    preferences_groups = {
        "First group": {"hide_help", "hide_help_3", "hide_help_4", "a_time_field"},
        "Second group": {"hide_help_2"},
    }

    preferences_groups_descriptions={
        "First group":"this is the first group, check the other one",
        "Second group":"this is the second group, check the other one",
    }

    hide_help = models.BooleanField(
        verbose_name=_("hide_help__verbose_name"),
        help_text=_("hide_help__help_text"),
        default=False,
    )
    hide_help_2 = models.BooleanField(
        verbose_name=_("hide_help_2__verbose_name"),
        help_text=_("hide_help__help_text"),
        default=False,
    )
    hide_help_3 = models.BooleanField(
        verbose_name=_("hide_help_3__verbose_name"),
        help_text=_("hide_help__help_text"),
        default=False,
    )
    hide_help_4 = models.BooleanField(
        verbose_name=_("hide_help_4__verbose_name"),
        help_text=_("hide_help__help_text"),
        default=False,
    )
    hide_help_5 = models.BooleanField(
        verbose_name=_("hide_help_5__verbose_name"),
        help_text=_("hide_help__help_text"),
        default=False,
    )
    a_time_field = models.TimeField(
        verbose_name=_("a_time_field__verbose_name"),
        help_text=_("a_time_field_%(tz)s__help_text") % dict(tz=settings.TIME_ZONE),
        null=True,
        blank=True,
    )
