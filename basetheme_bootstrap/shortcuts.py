from django.shortcuts import redirect
from urllib.parse import urlparse


def redirect_same_domain(to, *args, permanent=False, **kwargs):
    to = urlparse(to).path
    return redirect(to, *args, permanent=permanent, **kwargs)
