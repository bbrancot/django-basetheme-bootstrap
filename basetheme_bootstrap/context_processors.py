import logging

from django.conf import settings
from django.core.cache import cache

from basetheme_bootstrap.templatetags.basetheme_bootstrap import basetheme_bootstrap_template_if_not_redefined
from basetheme_bootstrap.user_preferences_utils import get_user_preferences_for_user

logger = logging.getLogger("basetheme_bootstrap")


def processors(request):
    return dict(
        basetheme_bootstrap_base_template=get_basetheme_bootstrap_base_template(),
        pref=get_user_preferences_for_user(None if request.user.is_anonymous else request.user),
        is_debug=settings.DEBUG,
    )


def get_basetheme_bootstrap_base_template():
    r = cache.get(
        'basetheme_bootstrap_base_template',
        default=None,
    )
    if r is None:
        r = basetheme_bootstrap_template_if_not_redefined("base.html")
        cache.set('basetheme_bootstrap_base_template', r, None)
    return r
