from django.contrib import admin
from django.contrib.admin.options import get_content_type_for_model
from django.urls import reverse
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext

from basetheme_bootstrap.user_preferences_utils import get_user_preference_class


class ViewOnSiteModelAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': ('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',)
        }

    def __init__(self, model, admin_site):
        if callable(getattr(model, "get_absolute_url", None)) and callable(self.view_on_site_in_list):
            self.list_display += ('view_on_site_in_list',)
        super().__init__(model, admin_site)

    def view_on_site_in_list(self, obj):
        return format_html(
            '<center><a href="' + reverse('admin:view_on_site', kwargs={
                'content_type_id': get_content_type_for_model(obj).pk,
                'object_id': obj.pk
            }) + '"><i class="fa fa-external-link"></i></a><center>')

    view_on_site_in_list.short_description = format_html('<center>' + gettext('View on site') + '<center>')


class UserPreferencesAdmin(admin.ModelAdmin):
    ordering = ('user',)
    list_display = ['username', ] + [
        field.name
        for field in (
            []
            if get_user_preference_class() is None
            else get_user_preference_class()._meta.get_fields()
        )
        if field.name not in [
            "id",
            "user",
        ] and not field.many_to_many
    ]
    list_filter = [
        field.name
        for field in (
            []
            if get_user_preference_class() is None
            else get_user_preference_class()._meta.get_fields()
        )
        if field.name not in [
            "id",
            "user",
        ]
    ]
    filter_horizontal = [field.name for field in (
        []
        if get_user_preference_class() is None
        else get_user_preference_class()._meta.get_fields()
    ) if field.many_to_many]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('user',)
        return self.readonly_fields

    def username(self, obj):
        return mark_safe("<i>default preferences</i>") if obj.user is None else obj.user

    username.admin_order_field = 'user__username'


try:
    admin.site.register(get_user_preference_class(), UserPreferencesAdmin)
except admin.sites.AlreadyRegistered as e:
    pass
except TypeError as e:
    pass
