from django.apps import AppConfig


class BasethemeBootstrapConfig(AppConfig):
    name = 'basetheme_bootstrap'
