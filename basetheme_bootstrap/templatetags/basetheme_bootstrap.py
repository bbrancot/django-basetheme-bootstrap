import logging
import re

from django import template
from django.conf import settings
from django.core.cache import cache
from django.template.base import Token
from django.template.loader import get_template
from django.template.loader_tags import do_include
from django.urls.base import reverse

register = template.Library()

logger = logging.getLogger(__name__)


@register.filter
def is_active_or_desc(request, pattern):
    try:
        if str(request.path).startswith(str(pattern)) \
                or str(request.path).startswith(str(reverse(pattern))):
            return 'active '
    except Exception:
        pass
    return ''


@register.filter
def is_active(request, pattern):
    try:
        if str(reverse(pattern)) == str(request.path) \
                or str(pattern) == str(request.path):
            return 'active '
    except Exception:
        pass
    return ''


@register.filter
def tags_to_bootstrap(tag):
    if tag == "error":
        return "danger"
    if tag == "":
        return "info"
    return tag


__group_name_to_id__pattern = re.compile('[\W_]+')


@register.filter
def group_name_to_id(group_name):
    return __group_name_to_id__pattern.sub('', str(group_name))


class IncludeIfExistsNode(template.Node):
    """
    A Node that instantiates an IncludeNode but wraps its render() in a
    try/except in case the template doesn't exist.
    """

    def __init__(self, parser, token):
        split_contents = token.split_contents()
        self.template_name = split_contents[1]
        self.template_node = do_include(parser, Token(token.token_type, 'do_include %s' % split_contents[1]))
        self.default_template_name = None
        self.default_template_node = None
        self.__template_seen=set()
        try:
            self.default_template_name = split_contents[2]
            self.default_template_node = do_include(parser,
                                                    Token(token.token_type, 'do_include %s' % split_contents[2]))
        except IndexError:
            pass

    def render(self, context):
        try:
            self.template_node.origin = self.origin
            return self.template_node.render(context)
        except template.TemplateDoesNotExist:
            cache_key = 'Template_not_found_%s' % self.template_name
            if cache.get(cache_key,   None) is None:
                cache.set(cache_key, '', None)
                self.__template_seen.add(self.template_name)
                print(self.__template_seen)
                logger.warning(
                    'Template %s was not found and could not be included.' % self.template_name +
                    (
                        ('Please see %s to have an example' % self.default_template_name)
                        if self.default_template_name else ''
                    )
                )
            if self.default_template_node:
                self.default_template_node.origin = self.origin
                return self.default_template_node.render(context)
            return ''


@register.tag
def include_if_exists(parser, token):
    """
    Include the specified template but only if it exists.
    """
    return IncludeIfExistsNode(parser, token)


@register.filter
def localize_template(template_name):
    return settings.BASETHEME_BOOTSTRAP_TEMPLATE_LOCATION_PROJECT + "/" + template_name


@register.filter
def basetheme_bootstrap_template_if_not_redefined(template_name):
    localized_template_name = localize_template(template_name)
    try:
        template.loader.get_template(localized_template_name)
        return localized_template_name
    except template.TemplateDoesNotExist:
        return "basetheme_bootstrap/" + template_name


@register.filter(name='get_class')
def get_class(value):
  return value.__class__.__name__