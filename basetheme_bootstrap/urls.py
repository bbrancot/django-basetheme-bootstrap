from django.urls import path, re_path
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy
from django.views.generic import RedirectView

from basetheme_bootstrap import views
from basetheme_bootstrap.forms import AuthenticationForm

app_name = 'basetheme_bootstrap'
urlpatterns = [
    ################################################################################
    # Account management
    ################################################################################
    path('accounts/', views.account_detail, name='account'),
    path('accounts/profile/', RedirectView.as_view(url=reverse_lazy('basetheme_bootstrap:account'))),
    path('accounts/login/', auth_views.LoginView.as_view(form_class=AuthenticationForm), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    path('accounts/password/', views.change_password, name='change_password'),
    path('accounts/signup/', views.signup, name='signup'),
    path('accounts/edit/', views.user_update, name='user-update'),
    path('accounts/remove/', views.user_delete, name='user-delete'),
    re_path('accounts/activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,32})/',
        views.activate, name='activate'),
    ################################################################################
    # Lost password
    ################################################################################
    path('accounts/password_reset/',
        auth_views.PasswordResetView.as_view(
            success_url=reverse_lazy("basetheme_bootstrap:password_reset_done"),
        ),
        name='password_reset'),
    path('accounts/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done'),
    re_path('accounts/password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,32})/',
        auth_views.PasswordResetConfirmView.as_view(
            success_url=reverse_lazy("basetheme_bootstrap:password_reset_complete"),
        ),
        name='password_reset_confirm'),
    path('accounts/password/reset/done/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete'),
    ################################################################################
    # About page
    ################################################################################
    path('about/', views.about_page, name='about_page'),
    ################################################################################
]
