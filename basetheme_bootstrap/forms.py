from django import forms
from django.contrib.auth import get_user_model, forms as auth_forms
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.forms import widgets
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from basetheme_bootstrap.default_settings import (
    is_username_is_email,
    is_first_last_name_required,
    get_failed_login_exponential_backoff_gradient,
    is_failed_login_exponential_backoff_enabled,
)


class CleanUsernameAndSuggestReset:
    class Meta:
        abstract = True

    def clean(self):
        f = super().clean()
        qs = get_user_model().objects
        if hasattr(self, 'instance'):
            qs = qs.filter(~Q(id=self.instance.id))
        if qs.filter(email=f["email"]).exists() or \
                not is_username_is_email() and \
                qs.filter(username=f.get("username", "")).exists():
            self.add_error("email", mark_safe(_(
                'The email already exists, if you have lost your password you can reset it '
                '<a href="%s">here</a>.') % (reverse('basetheme_bootstrap:password_reset'))))
        if is_first_last_name_required():
            if len(f.get("first_name", "")) == 0:
                self.add_error("email", _("First name is required"))
            if len(f.get("last_name", "")) == 0:
                self.add_error("email", _("Last name is required"))
        return f


class UserCreationFormWithMore(CleanUsernameAndSuggestReset, auth_forms.UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ("username", "email", "first_name", "last_name")
        field_classes = {'username': auth_forms.UsernameField}

    next = forms.CharField(widget=widgets.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(UserCreationFormWithMore, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        if is_username_is_email():
            del self.fields['username']
        if is_first_last_name_required():
            self.fields['first_name'].required = True
            self.fields['last_name'].required = True

    def save(self, commit=True):
        user = super().save(commit=False)
        if is_username_is_email():
            user.username = user.email
        if commit:
            user.save()
        return user


class MyUserChangeForm(CleanUsernameAndSuggestReset, auth_forms.UserChangeForm):
    password = None

    class Meta:
        model = get_user_model()
        fields = ("username", "email", "first_name", "last_name")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if is_username_is_email():
            del self.fields['username']
            self.fields['email'].required = True
        if is_first_last_name_required():
            self.fields['first_name'].required = True
            self.fields['last_name'].required = True

    def save(self, commit=True):
        user = super().save(commit=False)
        if is_username_is_email():
            user.username = user.email
        if commit:
            user.save()
        return user


class UserDeleteForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ()


class AuthenticationForm(auth_forms.AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if is_username_is_email():
            self.fields["username"].label = _("Email")

    def clean(self):
        if not is_failed_login_exponential_backoff_enabled():
            return super().clean()
        ip_address = self.request.META.get("REMOTE_ADDR")

        # Increment the login attempt count for this IP address
        cache_key = f"login_attempts:{ip_address}"
        login_attempts = cache.get(cache_key, 0)
        cache.set(cache_key, login_attempts + 1, 300)

        # check if lock exists for this ip address
        locked_key = f"locked_login_attempts:{ip_address}"
        locked_login_attempts = cache.get(locked_key, False)
        # expiry of the lock is based on the number of already done login_attempts
        wait_time = min(
            get_failed_login_exponential_backoff_gradient()
            ** login_attempts,
            300,
        )
        cache.set(locked_key, True, wait_time)

        # if locked was found we fail the form validation to prevent successful login
        if locked_login_attempts:
            m = int(wait_time // 60)
            s = int(wait_time - 60 * m)
            # informing the user that zhe has hit the wall and have to wait
            raise ValidationError(
                _(
                    "Too many login attempts ({:d}). Please try again later after {:02d}:{:02d} minute."
                ).format(login_attempts, m, s)
            )
        return super().clean()
