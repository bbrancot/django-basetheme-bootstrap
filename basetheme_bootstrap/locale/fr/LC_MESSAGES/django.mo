��    D      <  a   \      �     �     �     �               4     E     b     j     �  h   �  $   �          -     =     C     `     h  k   w  �   �     �  #   �     �     �  	   �  	   �  	    	  
   
	     	     ,	     1	     K	  {   c	     �	  	   �	     �	     
     /
     5
  :   <
     w
  $   �
     �
     �
     �
     �
  �   �
     �  a   �     �       `     #   �  1   �  R   �     )     ?     N  �   [  V   �  -   >    l  j   �  0   �  <     5   \  (   �  A  �  	   �                 :   =     x     �     �  #   �     �  s   �  (   O     x     �     �      �     �     �  k   �  �   Z       5   2     h       
   �  
   �  
   �     �     �     �     �     �  �        �     �     �  /   �  	          H   "     k  .   y     �     �     �  
   �  �   �     �  h   �     4     S  t   n  "   �  F     k   M     �     �     �  �   �  v   �  6   $  ?  [  �   �  :   #  J   ^  H   �  :   �        #      D   )   4                 !                      8       3                 >   "   =   -          (   '       $   ?   9   <   &      C   *              6       +       	          /          0                           7           @                 A   1      B          ,          2      5          :                        .   ;   
         %    About Account Account activated Account activation pending Account already active. Account deletion Account successfully created Actions Activation link is invalid! Admin An email was sent with a link to validate your account, please click on the link to enable your account. Bioinformatics and Biostatistics HUB Change my password Change password Close Connect or create an account Contact Create account Dear %(first_name)s %(last_name)s

Your account have successfully been created on %(joined)s.

Best regards Dear %(first_name)s %(last_name)s

Your account have successfully been created on %(joined)s.

Please click on the link to confirm your registration
%(activation_link)s

Best regards Default preferences Delete account and all related data Delete your account Email Error 403 Error 404 Error 500 First name First name is required Home I already have an account I am not registered yet If you don't receive an email, please make sure you've entered the address you registered with, and check your spam folder. L'Institut Pasteur Last name Last name is required Leave blank for default user Login Logout Please go to the following page and choose a new password: Preferences Preferences successfully saved at %s Reset my password Save changes Save my preferences Search Some data remaining in the system prevent the deletion of your account. Please either remove these data, or contact the administrator to solve the issue.
Message:
 Team Thank you for your email confirmation, you account have been activated and you are now logged in. Thanks for using our site! The %(site_name)s team The email already exists, if you have lost your password you can reset it <a href="%s">here</a>. The email has been addressed to %s. The resource you tried to access cannot be found. Too many login attempts ({:d}). Please try again later after {:02d}:{:02d} minute. Type your search here Update account View on site We've emailed you instructions for setting your password, if an account exists with the email you entered. You should receive them shortly. You are about to delete your account, all data linked to your account will be removed. You might want to contact the administrators. You want to delete your account, however not all data linked to your account can be removed.<br/>Hereafter
        are listed the objects preventing you account to be deleted. Please either manually delete them, or edit them so they
        do not remain linked to your account. You're receiving this email because you requested a password reset for your user account at %(site_name)s. Your are not authorised to access this resource. Your password has been set. You may go ahead and log in now. Your request cannot be process due to internal error. Your username, in case you've forgotten: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 À propos Compte Compte activé Activation du compte en attente Votre compte est déjà actif, vous pouvez vous connecter. Suppression de compte Compte créé avec succès Actions Le lien d'activation est invalide ! Admin Un email a été envoyé avec un lien pour valider votre compte, veuillez suivre le lien pour activer votre compte. Hub de Bioinformatique et Biostatistique Changer mon mot de passe Changer votre mot de passe Fermer Me connecter ou créer un compte Contacts Créer un compte Bonjour %(first_name)s %(last_name)s

Votre compte a été créé avec succès le %(joined)s.

Cordialement Bonjour %(first_name)s %(last_name)s

Votre compte a été créé avec succès le %(joined)s.

Veuillez suivre sur le lien pour confirmer votre inscription
%(activation_link)s

Cordialement Préférences par défaut Supprimer le compte et toutes les données associées Supprimer votre compte Courriel Erreur 403 Erreur 404 Erreur 500 Prénom Le prénom est requis Accueil J'ai déjà un compte Je ne suis pas encore inscrit.e Si vous ne recevez pas de courriel, assurez-vous d'avoir saisi l'adresse de courriel avec lequel vous vous êtes inscrit et vérifiez votre dossier de courrier indésirable. L'Institut Pasteur Nom Le nom est requis Laisser vide pour les préférences par défaut Connexion Deconnexion Veuillez aller sur la page ci-après et choisir un nouveau mot de passe: Préférences Préférences enregistrées avec succès à %s Réinitialiser mon mot de passe Enregistrer Sauvegarder mes préférences Rechercher Certaines données restantes dans le système empêchent la suppression de votre compte. Veuillez soit supprimer ces données, soit contacter l'administrateur pour résoudre le problème.
Message:
 Équipe Merci pour votre email de confirmation, votre compte a été activé et vous êtes maintenant connecté. Merci d'utiliser notre service L'équipe de %(site_name)s Le courriel existe déjà, si vous avez perdu votre mot de passe vous pouvez le réinitialiser <a href="%s">ici</a>. Le courriel à été envoyé à %s La ressource à laquelle vous avez essayé d'accéder est introuvable. Il y a trop de tentatives de connection ({:d}). Avant d'essayer de nouveau, merci d'attendre {:02d}m{:02d}s Rechercher ici Modifier votre compte Voir sur le site Nous vous avons envoyé par e-mail des instructions pour définir votre mot de passe, en supposant qu'un compte existe avec l'e-mail que vous avez entré. Vous devriez le recevoir sous peu. Vous êtes sur le points de supprimer votre compte, toutes les données liées à votre compte vont être supprimées. Vous devriez peut-être contacter les administrateurs. Vous souhaitez supprimer votre compte, cependant toutes les données liées à votre compte ne peuvent pas être supprimées.<br/> Ci-après
 sont listés les objets empêchant la suppression de votre compte. Veuillez soit les supprimer manuellement, soit les modifier afin qu'ils
 ne restent pas liés à votre compte. Vous recevez ce courriel car vous avez demandé une réinitialisation de mot de passe pour votre compte d'utilisateur à %(site_name)s. Vous n'êtes pas autorisé à accéder à cette ressource. Votre mot de passe a été défini. Vous pouvez maintenant vous connecter. Votre demande ne peut pas être traitée en raison d'une erreur interne. Votre nom d'utilisateur, au cas où vous l'auriez oublié: 