from django.conf import settings


def is_username_is_email():
    try:
        return settings.BASETHEME_BOOTSTRAP_USERNAME_IS_EMAIL
    except AttributeError:
        return False


def is_first_last_name_required():
    try:
        return settings.BASETHEME_BOOTSTRAP_FIRST_LAST_NAME_REQUIRED
    except AttributeError:
        return False


def is_validating_email():
    try:
        return settings.BASETHEME_BOOTSTRAP_VALIDATE_EMAIL_BEFORE_ACTIVATION
    except AttributeError:
        return False


def is_failed_login_exponential_backoff_enabled() -> bool:
    try:
        return settings.BASETHEME_BOOTSTRAP_FAILED_LOGIN_EXPONENTIAL_BACKOFF_ENABLED
    except AttributeError:
        return True


def get_failed_login_exponential_backoff_gradient() -> float:
    try:
        return settings.BASETHEME_BOOTSTRAP_FAILED_LOGIN_EXPONENTIAL_BACKOFF_GRADIENT
    except AttributeError:
        # import math
        # watch_period = 300
        # max_login_attempt = 15
        # return math.ceil(math.exp(math.log(watch_period) / max_login_attempt) * 100) / 100
        return 1.47
