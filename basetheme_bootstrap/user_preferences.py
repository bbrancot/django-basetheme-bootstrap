import logging

from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from basetheme_bootstrap import user_preferences_utils

logger = logging.getLogger("basetheme_bootstrap")

class UserPreferencesAbstractModel(user_preferences_utils.UserPreferencesAbstractModelWithoutUser):
    class Meta:
        abstract = True

    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="user_preferences",
        help_text=_("Leave blank for default user")
    )