import os
import re
from time import sleep

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser, Group
from django.core import mail
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.test import TestCase, RequestFactory, override_settings
from django.urls import reverse

from basetheme_bootstrap import user_preferences_utils, tokens, default_settings
from basetheme_bootstrap.user_preferences_utils import get_user_preferences_for_user


class AboutPageTests(TestCase):
    def test_works(self):
        response = self.client.get(reverse('basetheme_bootstrap:about_page'))
        self.assertEqual(response.status_code, 200)


class AccountPageTests(TestCase):
    def test_works(self):
        response = self.client.get(reverse('basetheme_bootstrap:account'))
        self.assertRedirects(
            response,
            expected_url=reverse('basetheme_bootstrap:login') + "?next=" + reverse('basetheme_bootstrap:account'),
        )


class SignUpTests(TestCase):

    def setUp(self):
        cache.clear()

    def test_works(self):
        response = self.client.get(reverse('basetheme_bootstrap:signup'))
        self.assertEqual(response.status_code, 200)

    def test_sign_up_form_view(self):
        user_count = get_user_model().objects.count()
        data = {
            'username': "userAAA",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('home'), )
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        user = get_user_model().objects.last()
        self.assertEqual(user.username, data["username"])
        self.assertEqual(user.email, data["email"])


class SignUpAttackWithNextParamTests(TestCase):

    def setUp(self):
        cache.clear()

    def test_legit_redirect(self):
        user_count = get_user_model().objects.count()
        data = {
            'username': "userAAA",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user",
            'next': reverse('basetheme_bootstrap:account'),
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data, follow=False)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('basetheme_bootstrap:account'), )
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        user = get_user_model().objects.last()
        self.assertEqual(user.username, data["username"])
        self.assertEqual(user.email, data["email"])

    def test_attacking_redirect(self):
        user_count = get_user_model().objects.count()
        data = {
            'username': "userAAA",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user",
            'next': "https://www.attacking.com" + reverse('basetheme_bootstrap:account'),
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data, follow=False)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('basetheme_bootstrap:account'), )
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        user = get_user_model().objects.last()
        self.assertEqual(user.username, data["username"])
        self.assertEqual(user.email, data["email"])


@override_settings(
    BASETHEME_BOOTSTRAP_USERNAME_IS_EMAIL=True,
)
class SignUpWithoutUsernameTests(TestCase):

    def test_sign_up_form_view(self):
        user_count = get_user_model().objects.count()
        data = {
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('home'), )
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        user = get_user_model().objects.last()
        self.assertEqual(user.username, data["email"])
        self.assertEqual(user.email, data["email"])

    def test_sign_up_detecte_duplicate_email(self):
        get_user_model().objects.create(username="toto", email="userAAA@mp.com")
        user_count = get_user_model().objects.count()
        data = {
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data)
        self.assertEqual(response.status_code, 200)


@override_settings(
    BASETHEME_BOOTSTRAP_FIRST_LAST_NAME_REQUIRED=True,
)
class SignUpWithFirstLastNameRequiredTests(TestCase):

    def test_sign_up_form_view(self):
        user_count = get_user_model().objects.count()
        data = {
            'username': "userAAA@mp.com",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "my_first_name",
            'last_name': "my_last_name",
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('home'), )
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        user = get_user_model().objects.last()
        self.assertEqual(user.username, data["email"])
        self.assertEqual(user.email, data["email"])
        self.assertEqual(user.first_name, data["first_name"])
        self.assertEqual(user.last_name, data["last_name"])

    def test_sign_up_detect_missing_last_name(self):
        data = {
            'username': "userAAA@mp.com",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "my_first_name",
            'last_name': "",
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data)
        self.assertEqual(response.status_code, 200)

    def test_sign_up_detect_missing_first_name(self):
        data = {
            'username': "userAAA@mp.com",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "",
            'last_name': "my_last_name",
        }
        response = self.client.post(reverse('basetheme_bootstrap:signup'), data)
        self.assertEqual(response.status_code, 200)


@override_settings(
    BASETHEME_BOOTSTRAP_VALIDATE_EMAIL_BEFORE_ACTIVATION=True,
    PASSWORD_RESET_TIMEOUT_DAYS=1,
    PASSWORD_RESET_TIMEOUT=60 * 60 * 24,
)
class SignUpWithValidationTests(TestCase):

    def setUp(self):
        cache.clear()
        get_user_model().objects.create(
            username="root",
        )
        self.data = {
            'username': "userAAA",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        }

    def test_sign_up_form_view(self):
        response = self.client.post(reverse('basetheme_bootstrap:signup'), self.data)
        self.assertEqual(response.status_code, 200)

        user = get_user_model().objects.last()
        self.assertFalse(user.is_active)
        self.assertIn(Group.objects.get(name="PendingAccountUser"), user.groups.all())

        self.assertEqual(len(mail.outbox), 1)
        activate_link_example = reverse('basetheme_bootstrap:activate', args=["AAA", "AAA-AAA"])
        activate_link_example = activate_link_example.replace("AAA", "([a-zA-Z0-9]+)")
        m = re.findall(activate_link_example, mail.outbox[0].body)
        self.assertEqual(len(m), 1)
        self.client.post(reverse('basetheme_bootstrap:activate', args=[m[0][0], m[0][1] + "-" + m[0][2]]))
        user = get_user_model().objects.last()
        self.assertTrue(user.is_active)
        self.assertNotIn(Group.objects.get(name="PendingAccountUser"), user.groups.all())

        ## test an account cannot be re-activated with the link:
        user.is_active = False
        user.save()
        self.client.post(reverse('basetheme_bootstrap:activate', args=[m[0][0], m[0][1] + "-" + m[0][2]]))
        self.assertFalse(get_user_model().objects.last().is_active)

    def test_sign_up_with_user_pending_resend_email(self):
        user_count = get_user_model().objects.count()
        response = self.client.post(reverse('basetheme_bootstrap:signup'), self.data)
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)

        response = self.client.post(reverse('basetheme_bootstrap:signup'), self.data)
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 2)
        activate_link_example = reverse('basetheme_bootstrap:activate', args=["AAA", "AAA-AAA"])
        activate_link_example = activate_link_example.replace("AAA", "([a-zA-Z0-9]+)")
        m = re.findall(activate_link_example, mail.outbox[-1].body)
        self.assertEqual(len(m), 1)

    def test_activate_too_late_with_user_pending_resend_email(self):
        actual_account_activation_token = tokens.account_activation_token

        class MockedTokenGenerator(tokens.TokenGenerator):
            def _today(self):
                from datetime import date, timedelta
                # Used for mocking in tests
                return date.today() - timedelta(days=2)

            def _now(self):
                from datetime import datetime, timedelta
                # Used for mocking in tests
                return datetime.now() - timedelta(days=2)

        tokens.account_activation_token = MockedTokenGenerator()

        user_count = get_user_model().objects.count()
        response = self.client.post(reverse('basetheme_bootstrap:signup'), self.data)
        self.assertEqual(get_user_model().objects.count(), user_count + 1)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(get_user_model().objects.last().is_active)

        tokens.account_activation_token = actual_account_activation_token

        self.assertEqual(len(mail.outbox), 1)
        activate_link_example = reverse('basetheme_bootstrap:activate', args=["AAA", "AAA-AAA"])
        activate_link_example = activate_link_example.replace("AAA", "([a-zA-Z0-9]+)")
        m = re.findall(activate_link_example, mail.outbox[0].body)
        self.assertEqual(len(m), 1)
        self.client.post(reverse('basetheme_bootstrap:activate', args=[m[0][0], m[0][1] + "-" + m[0][2]]))

        self.assertFalse(get_user_model().objects.last().is_active)
        self.assertEqual(len(mail.outbox), 2)
        activate_link_example = reverse('basetheme_bootstrap:activate', args=["AAA", "AAA-AAA"])
        activate_link_example = activate_link_example.replace("AAA", "([a-zA-Z0-9]+)")
        m = re.findall(activate_link_example, mail.outbox[1].body)
        self.assertEqual(len(m), 1)


class TestWithTemplatesInPlace(SignUpTests):

    def setUp(self):
        super().setUp()

    def test_with_templates(self):
        templates = [
            "test_app_1/templates/test_app_1/fork_me.html",
            "test_app_1/templates/test_app_1/nav_bar.html",
            "test_app_1/templates/test_app_1/last_update.html",
        ]

        for t in templates:
            open(t, 'a').close()
        self.test_works()
        self.test_sign_up_form_view()
        for t in templates:
            os.remove(t)


@override_settings(
    BASETHEME_BOOTSTRAP_VALIDATE_EMAIL_BEFORE_ACTIVATION=True,
)
class SuperuserSignUpTests(TestCase):
    def test_sign_up_for_superuser(self):
        # even with BASETHEME_BOOTSTRAP_VALIDATE_EMAIL_BEFORE_ACTIVATION to True first user must be super and active
        response = self.client.post(reverse('basetheme_bootstrap:signup'), {
            'username': "userAAA",
            'email': "bryan.brancotte@pasteur.fr",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        })
        self.assertEqual(response.status_code, 302)
        self.assertTrue(get_user_model().objects.get(username="userAAA").is_superuser)
        self.assertTrue(get_user_model().objects.get(username="userAAA").is_active)


class SuperuserSignUpTests(TestCase):

    def setUp(self):
        cache.clear()

    def test_sign_up_for_superuser(self):
        response = self.client.post(reverse('basetheme_bootstrap:signup'), {
            'username': "userAAA",
            'email': "bryan.brancotte@pasteur.fr",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('home'), )
        self.assertTrue(get_user_model().objects.get(username="userAAA").is_superuser)

        response = self.client.post(reverse('basetheme_bootstrap:signup'), {
            'username': "userAAAZZZ",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('home'), )
        self.assertEqual(get_user_model().objects.count(), 2)
        self.assertFalse(get_user_model().objects.get(username="userAAAZZZ").is_superuser)


class ChangePasswordTests(TestCase):

    def setUp(self):
        cache.clear()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"
        self.user_pwd_2 = "aig7thah4eethahdaDae"
        self.user = get_user_model().objects.create(
            username="a",
            email="a@a.a",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

    def test_works(self):
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))
        response = self.client.get(reverse('basetheme_bootstrap:change_password'))
        self.assertEqual(response.status_code, 200)

    def test_changepassword_ok_view(self):
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))

        response = self.client.post(reverse('basetheme_bootstrap:change_password'), {
            'old_password': self.user_pwd,
            'new_password1': self.user_pwd_2,
            'new_password2': self.user_pwd_2,
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('basetheme_bootstrap:account'), )
        self.client.logout()
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd_2))

    def test_changepassword_fail_view(self):
        user_login = self.client.login(username="a", email="a@a.a", password=self.user_pwd)
        self.assertTrue(user_login)
        response = self.client.post(reverse('basetheme_bootstrap:change_password'), {
            'old_password': "A",
            'new_password1': "Z",
            'new_password2': "E",
        })
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))

        user_login = self.client.login(username="a", email="a@a.a", password=self.user_pwd)
        self.assertTrue(user_login)
        response = self.client.post(reverse('basetheme_bootstrap:change_password'), {
            'old_password': self.user_pwd,
            'new_password1': "Z",
            'new_password2': "E",
        })
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))


class ChangeFirstnameTests(TestCase):

    def setUp(self):
        cache.clear()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"
        self.user_pwd_2 = "aig7thah4eethahdaDae"
        self.user = get_user_model().objects.create(
            username="a",
            email="aa@aa.aa",
            first_name="tata",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

    def test_works(self):
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))
        response = self.client.get(reverse('basetheme_bootstrap:user-update'))
        self.assertEqual(response.status_code, 200)

    def test_empty_username(self):
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))
        user = get_user_model().objects.get(username="a")
        response = self.client.post(reverse('basetheme_bootstrap:user-update'), {
            'username': "",
            'email': user.email,
            'first_name': "toto",
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(get_user_model().objects.get(pk=user.pk).username, "a")

    def test_change_firstname(self):
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))

        self.assertEqual(get_user_model().objects.get(username="a").first_name, "tata")
        response = self.client.post(reverse('basetheme_bootstrap:user-update'), {
            'username': get_user_model().objects.get(username="a").username,
            'email': get_user_model().objects.get(username="a").email,
            'first_name': "toto",
        })
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, expected_url=reverse('basetheme_bootstrap:account'), )
        self.client.logout()
        self.assertEqual(get_user_model().objects.get(username="a").first_name, "toto")


class DeleteAccountTests(TestCase):

    def setUp(self):
        cache.clear()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"
        self.user_pwd_2 = "aig7thah4eethahdaDae"
        self.user = get_user_model().objects.create(
            username="a",
            email="aa@aa.aa",
            first_name="tata",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

    def test_works(self):
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))
        response = self.client.get(reverse('basetheme_bootstrap:user-delete'))
        self.assertEqual(response.status_code, 200)

    def test_works_post(self):
        self.assertTrue(self.client.login(username="a", email="a@a.a", password=self.user_pwd))
        response = self.client.post(reverse('basetheme_bootstrap:user-delete'), {})
        self.assertEqual(response.status_code, 302)
        self.assertIn(response.url, ['/', reverse('basetheme_bootstrap:login')])
        # self.assertRedirects(response, expected_url='/', )


class TemplatesTagsTests(TestCase):
    def setUp(self):
        cache.clear()
        self.factory = RequestFactory()

    def test_is_active_or_desc(self):
        from basetheme_bootstrap.templatetags.basetheme_bootstrap import is_active_or_desc
        request = self.factory.get(reverse("basetheme_bootstrap:change_password"))
        self.assertEqual('active ', is_active_or_desc(
            request,
            "basetheme_bootstrap:account"
        ))
        self.assertEqual('active ', is_active_or_desc(
            request,
            "basetheme_bootstrap:change_password"
        ))
        request = self.factory.get(reverse("basetheme_bootstrap:account"))
        self.assertEqual('', is_active_or_desc(
            request,
            "basetheme_bootstrap:change_password"
        ))
        self.assertEqual('', is_active_or_desc(
            None,
            "basetheme_bootstrap:account"
        ))
        self.assertEqual('', is_active_or_desc(
            request,
            None
        ))

    def test_is_active(self):
        from basetheme_bootstrap.templatetags.basetheme_bootstrap import is_active
        request = self.factory.get(reverse("basetheme_bootstrap:change_password"))
        self.assertEqual('', is_active(
            request,
            "basetheme_bootstrap:account"
        ))
        self.assertEqual('active ', is_active(
            request,
            "basetheme_bootstrap:change_password"
        ))
        request = self.factory.get(reverse("basetheme_bootstrap:account"))
        self.assertEqual('', is_active(
            request,
            "basetheme_bootstrap:change_password"
        ))
        self.assertEqual('', is_active(
            None,
            "basetheme_bootstrap:account"
        ))
        self.assertEqual('', is_active(
            request,
            None
        ))

    def test_tags_to_bootstrap(self):
        from basetheme_bootstrap.templatetags.basetheme_bootstrap import tags_to_bootstrap
        self.assertEqual(tags_to_bootstrap("error"), "danger")
        self.assertEqual(tags_to_bootstrap("warning"), "warning")
        self.assertEqual(tags_to_bootstrap(""), "info")


class UserPreferencesTests(TestCase):

    def setUp(self):
        cache.clear()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"
        self.user = get_user_model().objects.create(
            username="user",
            email="aa@aa.aa",
            first_name="tata",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

    def test_when_enabled_that_it_works(self):
        if not getattr(settings, "BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_ENABLED", False):
            return
        self.assertIsNotNone(user_preferences_utils.get_user_preference_class())
        pref = user_preferences_utils.get_user_preferences_for_user(self.user)
        self.assertIsNotNone(pref)
        default_pref = user_preferences_utils.get_user_preferences_for_user(None)
        self.assertIsNotNone(default_pref)
        self.assertNotEqual(pref, default_pref)

    def test_when_enabled_that_it_works_2(self):
        if not getattr(settings, "BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_ENABLED", False):
            return
        default_pref = user_preferences_utils.get_user_preferences_for_user(None)
        self.assertIsNotNone(default_pref)
        pref = user_preferences_utils.get_user_preferences_for_user(self.user)
        self.assertIsNotNone(pref)
        self.assertNotEqual(pref, default_pref)

        default_pref.user = self.user
        default_pref.save()
        default_pref.user = None
        default_pref.save()
        pref.save()
        str(pref)

        default_pref = user_preferences_utils.get_user_preferences_for_user(None)
        self.assertIsNotNone(default_pref)
        pref = user_preferences_utils.get_user_preferences_for_user(self.user)
        self.assertIsNotNone(pref)
        self.assertNotEqual(pref, default_pref)

    def test_only_one_default_pref(self):
        if not getattr(settings, "BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_ENABLED", False):
            return
        pref = user_preferences_utils.get_user_preferences_for_user(self.user)
        pref.user = None
        self.assertRaises(ValidationError, pref.save)

    def test_AnonymousUser_do_not_crash(self):
        get_user_preferences_for_user(AnonymousUser())

    def test_caching_disabled_status_when_it_is_the_case(self):
        user_preferences_utils.get_user_preference_class()
        self.assertTrue(
            cache.get("has_no_user_preferences_model", False) or
            getattr(settings, "BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_ENABLED", False)
        )


@override_settings(
    BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_ENABLED=False,
)
class SignUpTestsUserPrefDown(SignUpTests):
    pass

    def test_dummy(self):
        cache.clear()
        self.client.post(reverse('basetheme_bootstrap:signup'), {
            'username': "userAAA",
            'email': "userAAA@mp.com",
            'password1': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'password2': "user@mp.comuser@mp.comuser@mp.comuser@mp.com",
            'first_name': "user"
        })
        user_preferences_utils.get_user_preference_class()
        user_preferences_utils.get_user_preference_class()
        user_preferences_utils.get_user_preferences_for_user(get_user_model().objects.first())

    def setUp(self):
        cache.clear()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"
        self.user = get_user_model().objects.create(
            username="user",
            email="aa@aa.aa",
            first_name="tata",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

    def test_view_account_without_pref(self):
        cache.clear()
        self.client.force_login(self.user)
        response = self.client.get(reverse('basetheme_bootstrap:account'))
        self.assertEqual(response.status_code, 200)


@override_settings(
    BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_NAME="AAAAA",
)
class SignUpTestsUserWrongUserPrefLocation(SignUpTests):
    pass


@override_settings(
    DEBUG=False,
)
class AboutPageTestsDebugFalse(AboutPageTests):
    pass


@override_settings(
    DEBUG=False,
)
class SignUpTestsDebugFalse(SignUpTests):
    pass


@override_settings(
    DEBUG=False,
)
class SuperuserSignUpTestsDebugFalse(SuperuserSignUpTests):
    pass


@override_settings(
    DEBUG=False,
)
class ChangePasswordTestsDebugFalse(ChangePasswordTests):
    pass


@override_settings(
    DEBUG=False,
)
class ChangeFirstnameTestsDebugFalse(ChangeFirstnameTests):
    pass


@override_settings(
    DEBUG=False,
)
class DeleteAccountTestsDebugFalse(DeleteAccountTests):
    pass


@override_settings(
    DEBUG=False,
)
class TemplatesTagsTestsDebugFalse(TemplatesTagsTests):
    pass


@override_settings(
    DEBUG=False,
)
class UserPreferencesTestsDebugFalse(UserPreferencesTests):
    pass


class ExponentialBackoffFailedLoginEnabledTests(TestCase):
    def test_basic(self):
        self.assertTrue(default_settings.is_failed_login_exponential_backoff_enabled())


@override_settings(
    BASETHEME_BOOTSTRAP_FAILED_LOGIN_EXPONENTIAL_BACKOFF_ENABLED=False,
)
class ExponentialBackoffFailedLoginDisabledTests(TestCase):
    def test_basic(self):
        self.assertFalse(default_settings.is_failed_login_exponential_backoff_enabled())


@override_settings(
    BASETHEME_BOOTSTRAP_FAILED_LOGIN_EXPONENTIAL_BACKOFF_ENABLED=True,
)
class ExponentialBackoffFailedLoginEnabledTests(TestCase):
    def test_basic(self):
        self.assertTrue(default_settings.is_failed_login_exponential_backoff_enabled())


class ExponentialBackoffFailedLoginTests(TestCase):
    def setUp(self):
        cache.clear()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"
        self.user = get_user_model().objects.create(
            username="a",
            email="a@a.a",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

    def test_login_ok(self):
        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': self.user_pwd,
        }
        response = self.client.post(reverse('basetheme_bootstrap:login'), data, follow=False)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.wsgi_request.user.is_authenticated)

    def test_works(self):
        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': "NOT IT",
        }
        response = self.client.post(reverse('basetheme_bootstrap:login'), data, follow=False)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.wsgi_request.user.is_authenticated)

        for i in range(10):
            self.client.post(reverse('basetheme_bootstrap:login'), data, follow=False)

        response = self.client.post(reverse('basetheme_bootstrap:login'), data, follow=False)
        self.assertFalse(response.wsgi_request.user.is_authenticated)


@override_settings(
    BASETHEME_BOOTSTRAP_FAILED_LOGIN_EXPONENTIAL_BACKOFF_GRADIENT=1,
)
class ExponentialBackoffFailedLoginAfterDelayTests(TestCase):
    def setUp(self):
        cache.clear()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"
        self.user = get_user_model().objects.create(
            username="a",
            email="a@a.a",
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

    def test_works(self):
        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': "NOT IT",
        }

        for i in range(10):
            response = self.client.post(reverse('basetheme_bootstrap:login'), data, follow=False)
            self.assertFalse(response.wsgi_request.user.is_authenticated)

        sleep(2)

        response = self.client.post(
            reverse('basetheme_bootstrap:login'),
            data={
                'username': self.user.username,
                'email': self.user.email,
                'password': self.user_pwd,
            },
            follow=False,
        )
        self.assertTrue(response.wsgi_request.user.is_authenticated)
